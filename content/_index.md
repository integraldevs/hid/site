We are people dedicated to promote the health of human beings in all possible dimensions of their life, restoring ambients and providing tools for self-healing and knowledge from an *integral* vision of life, by improving every aspect affecting its quality.

Between the holistic approach of the integral revolution and the world-inclusive approach of the integral-theory we propose Integral as a Conceptual Framework in which to develop politics and tools to foster "common" ethical values.

Research on those common ethical values and how to implement them is an ongoing process.

In order to build up a healthy team to work on goals as explained above, we set up the following points as the current results of an organic and always open collective process of improving and broadening them. 

---

1. **Art as an attitude in front of any task.** An attitude is artistic when it makes doers to enjoy and improve any action: *All verbs are Arts!* 
This way, enjoing-improving each one of the tasks we perform, enhances the quality of both experience and whatever is the outcomes of that action.

2. All things produced by humans are then Artworks, and they can be nutritive, empty or toxic. The nutritive value of Artworks is related to its ability to enhance healthness for the majority of humankind and nature. *Whether Artworks have toxic or empty value we chose to keep them away.*

3. **Nature as a reference and constant nutritive teacher**. As Nature, Life is complex but we've lost the fear about complexity and we are open to embrace its management. Every being is singular therefore we avoid simplification and standardization. *There are no uniform equal parts in nature!*

4. We took seriously the fact that **a personal evolution is mandatory to evolve collectively as a species**; from an ego-centric to a world-centric point of view. We also believe is necessary to keep our inner source of love open to avoid falling into the endless quest for love outside of ourselves!

5. The mental ability of separate the reality in parts or words (ultimately, the Ego), alone, brings us to loneliness and collective disaster. **It is required to reestablish the mental ability of unifying and finding what’s common in all stuff** (the empathic Consciousness) if we want to create communities and survive as a specie. _We drive from our consciousness and we keep the ego as the best possible copilot (but a bad driver)!_

6. Only love creates, unifies and builds (when is unconditional, altruist). The fear and the egoism (otherwise natural when too young) separates ourselves and destroys. **Only when we stop judging we can love and communicate in a healthy way**, accepting the reality as it is in order to improve it.

7. **The common good tools needs to be funded by the common people**, everyone with his/her possibilities, because they will never be funded by the neo-liberal capitalism. _We can organize ourselves to solve the common needs of the people!_

8. The most effective way to facilitate a global change to Love (once it is generated individually) is by **creating and feeding new and better common structures**, better education and economies (fair, ecologic, cooperative, social and solidary), and stop feeding an old system, obsolete, unfair and antidemocratic, that is falling anyway.

9. Take the decision to have real **commitment** with one-self and with others, to became **coherent** (doing what you say) and create links and community.

10. There are two kinds of **authority**, the one of ruling and the one of knowing. We foster the partial leaderships of the ones who knows best on each matter by experience, and we reject the authority of who rules from ignorance or just theory.

