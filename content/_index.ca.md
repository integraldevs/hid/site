Som gent dedicada a fomentar la salut dels éssers vius en totes les dimensions possibles, restaurant ambients i facilitant èines d'autosanació i coneixement des d'una visió integral de la vida, millorant tots els aspectes que afecten la seva qualitat.

---

### Principis bàsics:

1. **L’art com a actitud davant qualsevol tasca.** L’actitud artística ens permet gaudir i millorar qualsevol acció: *Tots els Verbs son Arts!*

2. Totes les coses son doncs obres d’art que poden ser nutritives, buides o tòxiques. Les coses son sanes i afavoreixen la salut quan son nutritives per a molta gent (no només per l’autor). *Evitem les obres buides o tòxiques!*

3. **La natura com a referent i mestre constant.** La vida és complexe i necessitem perdre la por a la complexitat i obrir-nos a la seva gestió. Cada ser és singular i no podem seguir simplificant i uniformitzant-ho tot al estil industrial. *No existeixen les ‘parts iguals’ a la natura!*

4. **Cal evolucionar personalment** per poder evolucionar col·lectivament com a espècie, anant des del punt de vista ego-cèntric i etno-cèntric al mundi-cèntric. És vital obrir la pròpia font d’amor dins de cadascú per deixar de buscar l’amor a fora d’un mateix!

5. La capacitat mental de separar la realitat en paraules (hemisferi esquerra, l’Ego) per sí sola ens porta a la soledat i al desastre col·lectiu. **Cal restablir la capacitat mental d’unir i trobar allò comú en tot** (hemisferi dret, la Consciencia) si volem crear comunitats i sobreviure com a espècie. *Pilotem amb la consciencia i mantenim l’ego com a simple copilot!*

6. Només l’amor crea, uneix i construeix (quan és incondicional, altruista). La por i l’egoisme infantil ens separa i destrueix. **Només deixant de jutjar podem estimar i comunicar-nos de forma sana**, acceptant la realitat tal com és per poder-la millorar.

7. **Les èines pel bé comú cal finançar-les entre tots**, cadascú segons les seves possibilitats, doncs no seran mai finançades pel capitalisme neoliberal. Podem organitzar-nos col·lectivament per resoldre les necessitats comuns de les persones!

8. La forma més efectiva de facilitar un canvi global cap a l’Amor (un cop generat individualment) és **creant i alimentant noves i millors estructures comuns**, millor educació, millors economies (justes, ecològiques, cooperatives, socials i solidàries), deixant d’alimentar un sistema antic, obsolet, injust i anti-democràtic que ja cau per sí mateix.

9. Prendre la desició del **compromís** cap a un mateix i cap als altres per ser **coherent** (fer allò que dius), crear vincles i comunitat.

10. Hi han **dos tipus d’autoritat**: la del manar i la del saber. Abracem els lideratges (parcials) dels qui saben més en cada tema per experiencia, i rebutjem l’autoritat dels qui manen desde la ignorància.

El procés col·lectiu de millorar i ampliar aquests punts és orgànic i sempre obert.
