---
title: Sumate al movimiento
subtitle: Hazte socio de la asociación y ayuda a construir el nuevo bien común
comments: false
---

![image alt text](/img/dualflow.png)

Para financiar los objectivos de la asociación nos hace falta generar entre todas un **Bote Común Integral** de recursos de todo tipo (dinero, productos, horas, herramientas, etc).

Para **hacerse socio de la asociación**, colaborando con el desarrollo de la red p2p [Common Database](https://commondb.net) y particpar en las actividades del [Espacio Transparente](https://espaitransparent.art), solo hay que llenar el formulario OCP de [Registro a la SDI](https://join.integral.tools).

Para conseguir los objectivos de mejorar la calidad de vida de todas os invitamos a organizarnos con las necesidades y los recursos, llenando el formulario del **Registro Integral de Necesidades y Recursos para la Autogestión Local** que encontrareis en el espacio (próximamente online!).

![image alt text](/img/ET_logo_ca.png)
