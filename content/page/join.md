---
title: Join the movement
subtitle: Be a member of the asociation and help rebuilding the common good
comments: false
---

![image alt text](/img/dualflow.png)

To **become a member of the association** and participate of all activities at [Espai Transparent](https://espaitransparent.art) or contribute to the development of the [Common Database](https://commondb.net) P2P network, you only need to fill up our actual [Register form](https://join.integral.tools) (using OCP).

To fund the goals of the association we need to generate together a **Common Integral Pot** of resources of any kind (money, products, hours, tools, etc).

To reach the goal of improving the life quality of everyone, we invite you to organize ourselves about the needs, filling in the form of the "Integral Register of Needs and Resources for the Local Self-management" that you’ll find physically in our local space (online version coming soon).

![image alt text](/img/ET_logo_ca.png)
