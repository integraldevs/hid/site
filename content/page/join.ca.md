---
title: Suma't al moviment
subtitle: Fes-te soci de l'associació i ajuda a construir el nou be comú
comments: false
---

![image alt text](/img/dualflow.png)

Per finançar els objectius de l’associació ens cal generar entre tots un **Pot Comú Integral** de recursos de tota mena (diners, productes, hores, eines, etc).

Per **fer-se soci de l'associació** i contribuir en el desenvolupament de la xarxa P2P [Common Database](https://commondb.net) o participar de les activitats de l'[Espai Transparent](https://espaitransparent.art), només cal omplir el formulari OCP de [Registre a la SDI](https://join.integral.tools).

Per assolir els objectius de millorar la qualitat de vida de tothom us convidem a organitzar-nos amb les necessitats i els recursos, omplint el formulari del **Registre Integral de Necessitats i Recursos per l’Autogestió Local** que trobareu a l’espai (properament online!).

![image alt text](/img/ET_logo_ca.png)
