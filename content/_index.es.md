Somos gente dedicada a fomentar la salud de los seres vivos en todas las dimensiones posibles, restaurando ambientes y facilitando herramientas de autosanación y de conocimiento desde una visión integral de la vida, mejorando todos los aspectos que afectan su calidad.

---

1. **El arte como actitud ante cualquier tarea.** La actitud artística nos permite disfrutar y mejorar cualquier acción: *Todos los Verbos son Artes!*

2. Todas las cosas son pues obras de arte que pueden ser nutritivas, vacías o tóxicas. Las cosas son sanas y favorecen la salud cuando son nutritivas para mucha gente (no solo para el autor). *Evitemos las obras vacías o tóxicas!*

3. La naturaleza como referente y maestra constante. La vida es compleja y necesitamos perder el miedo a la complejidad y abrirnos a su gestión. Cada ser es singular y no podemos seguir simplificando y uniformando todo al estilo industrial. No existen las ‘partes iguales’ en la naturaleza!

4. **Hace falta evolucionar personalmente** para poder evolucionar colectivamente como especie. Es vital abrir la propia fuente de amor dentro de cada uno para dejar de buscar el amor a fuera de uno mismo!

5. La capacidad mental de separar la realidad en palabras (hemisferio izquierdo, el Ego) por sí sola nos lleva a la soledad y al desastre colectivo. **Hace falta restablecer la capacidad mental de unir y encontrar aquello común en todo** (hemisferio derecho, la Consciencia) si queremos crear comunidades y sobrevivir como especie. Pilotamos con la consciencia y mantenemos el ego como simple copiloto!

6. Solo el amor crea, une y construye (cuando es incondicional, altruista). El miedo y el egoísmo infantil nos separa y destruye. **Solo dejando de juzgar podemos amar y comunicarnos de forma sana,** aceptando la realidad tal como es para poderla mejorar.

7. **El bien común hace falta financiarlo entre todos,** cada uno según sus posibilidades, pues nunca será financiado por el capitalismo neo-liberal. Podemos organizarnos colectivamente para resolver las necesidades comunes de las personas!

8. La forma mas efectiva de facilitar un cambio global hacia el Amor (un vez producido individualmente) es **creando y alimentando nuevas y mejores estructuras comunes**, mejor educación, mejores economías (justas, ecológicas, cooperativas, sociales y solidarias), dejando de alimentar un sistema antiguo, obsoleto, injusto y anti-democrático que ya cae por sí mismo.

9. Tomar la desición del **compromiso** hacia uno mismo y hacia los demás para ser **coherente** (hacer aquello que dices), crear vinculos y comunidad.

10. Hay dos tipos de **autoridad**: la del mandar y la del saber. Abrazamos los liderazgos (siempre parciales) de quienes saben más de cada tema por experiencia, y rechazamos la autoridad de quienes mandan desde la ignorancia.

