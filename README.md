Health and Integral Development is a non-profit association devoted to promoting human well-being at all levels, to collectively identify tools and tactics in an attempt to improve life in each of the aspects that affect its quality.

We aim to develop different projects to pursue the common good under the flag of **open source philosophy**, thus promoting what we named art attitudes such as the pleasure of doing, sharing and collaborating and other open-source practice commons to both the hacker  and activism movement in their genuine essence. 

**Integral** means that between the holistic approach implied in the concept of integral revolution and the world-inclusive approach of the [integral-theory](https://en.wikipedia.org/wiki/Integral_theory_(Ken_Wilber)) we propose Integral as a Conceptual Framework in which to develop politics and tools to foster "common" ethical values.
Research on those common ethical values and how to implement them is an ongoing process.

In order to build up a healthy team to work on goals as explained above, we set up the following points as the current results of an organic and always open collective process of improving and broadening them:

Art as an attitude in front of any task. An attitude is artistic when it makes doers enjoy and improve any action: All verbs are Arts! This way, enjoying-improving each one of the tasks we perform enhances the level of quality of both experiences and whatever is the outcomes of that action.

We took seriously the fact that personal evolution is mandatory to evolve collectively as a species; from an ego-centric to a world-centric point of view. We also believe is necessary to keep our inner source of love open to avoid falling into the endless quest for love outside of ourselves!

The practice of open source, in addition to establishing a model of collaboration and commitment, means choosing a mindset whose foundations are openness, attribution and comprehensive references to meet the conditions of coherence and facilitate a set of tools to work with rather than pre-packaged ideas.


------------


![Build Status](https://gitlab.com/pages/hugo/badges/master/build.svg)

---

Example [Hugo] website using GitLab Pages.

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
- [Building locally](#building-locally)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)
- [Did you fork this project?](#did-you-fork-this-project)
- [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] Hugo
1. Preview your project: `hugo server`
1. Add content
1. Generate the website: `hugo` (optional)

Read more at Hugo's [documentation][].

### Preview your site

If you clone or download this project to your local computer and run `hugo server`,
your site can be accessed under `localhost:1313/hugo/`.

The theme used is adapted from http://themes.gohugo.io/beautifulhugo/.

## GitLab User or Group Pages

To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

You'll need to configure your site too: change this line
in your `config.toml`, from `"https://pages.gitlab.io/hugo/"` to `baseurl = "https://namespace.gitlab.io"`.
Proceed equally if you are using a [custom domain][post]: `baseurl = "http(s)://example.com"`.

Read more about [user/group Pages][userpages] and [project Pages][projpages].

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

## Troubleshooting

1. CSS is missing! That means two things:

    Either that you have wrongly set up the CSS URL in your templates, or
    your static generator has a configuration option that needs to be explicitly
    set in order to serve static assets under a relative URL.

[ci]: https://about.gitlab.com/gitlab-ci/
[hugo]: https://gohugo.io
[install]: https://gohugo.io/overview/installing/
[documentation]: https://gohugo.io/overview/introduction/
[userpages]: http://doc.gitlab.com/ee/pages/README.html#user-or-group-pages
[projpages]: http://doc.gitlab.com/ee/pages/README.html#project-pages
[post]: https://about.gitlab.com/2016/04/07/gitlab-pages-setup/#custom-domains
